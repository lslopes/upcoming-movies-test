//
//  UMShowcaseListPresenterPresenter.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright (c) 2018 Leandro Lopes. All rights reserved.
//

import Foundation

// MARK: - UMShowcaseListPresenterPresenter
class UMShowcaseListPresenter {

    // MARK: Instance Variables

    var interactor: UMShowcaseListInteractor!
    weak var view: UMShowcaseListViewInterface! {
        didSet{
            setupInterface()
        }
    }
    init(){}
    init(view: UMShowcaseListViewInterface) {
        self.view = view
        setupInterface()
    }
    func setupInterface(){
        let thisInteractor = UMShowcaseListInteractor()
        self.interactor = thisInteractor
        thisInteractor.output = self
    }

}

// MARK: - UMShowcaseListPresenterModuleInterface
extension UMShowcaseListPresenter: UMUpcomingListModuleInterface {
    
    func searchMovie(withTitle title: String, page: Int) {
        self.interactor.searchMovie(text: title, page: page)
    }
    
    func getUpcomingMovies(page: Int) {
        self.interactor.getUpcomingMoviesList(page: page)
    }
    

}

// MARK: - UMShowcaseListPresenterInteractorOutput
extension UMShowcaseListPresenter: UMShowcaseListInteractorOutput {
    
    func didGetMoviesList(data: UpcomingMovieListModel) {
        self.view.didFetchMoviesList(data: data)
    }
    
    func didSearchMovies(data: UpcomingMovieListModel) {
        self.view.didFetchMoviesList(data: data)
    }
    
    func didReceivedError(errorString: String) {
        
    }
    

}
