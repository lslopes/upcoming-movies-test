//
//  UMShowcaseListPresenterInteractor.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright (c) 2018 Leandro Lopes. All rights reserved.
//

import Foundation

// MARK: - UMShowcaseListPresenterInteractorInput
protocol UMShowcaseListInteractorInput: class {
    
    func getUpcomingMoviesList(page: Int)
    func searchMovie(text: String, page: Int)
    
}

// MARK: - UMShowcaseListPresenterInteractorOutput
protocol UMShowcaseListInteractorOutput: ViewInteractorOutput {

    func didGetMoviesList(data: UpcomingMovieListModel)
    
}

// MARK: - UMShowcaseListPresenterInteractor
class UMShowcaseListInteractor {

    // MARK: Instance Variables
    var output: UMShowcaseListInteractorOutput?
    fileprivate var dataTask: URLSessionDataTask?

}

// MARK: - Extension UMShowcaseListPresenterInteractorInput
extension UMShowcaseListInteractor: UMShowcaseListInteractorInput {
    
    func searchMovie(text: String, page: Int) {
        
        guard let url = URL(string: "\(API_URL)search/movie?api_key=\(API_KEY_V3)&query=\(text)&page=\(page)&include_adult=false") else { return }
        
        let session = URLSession(configuration: .default)
        dataTask = session.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                
                self.output?.didReceivedError(errorString: "#request failed with error description: \(String(describing: error?.localizedDescription))")
                
            } else {
                
                let httpResponse = response as? HTTPURLResponse
                guard httpResponse?.statusCode == 200 else {
                    self.output?.didReceivedError(errorString: "#request failed with status: \(String(describing: httpResponse?.statusCode))")
                    return
                }
                
                if let a = try? JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let model = UpcomingMovieListModel.buildFrom(json: a!)
                    self.output?.didGetMoviesList(data: model)
                }
                
            }
        })
        
        dataTask?.resume()
        
    }
    

    func getUpcomingMoviesList(page: Int) {

        guard let url = URL(string: "\(API_URL)movie/upcoming?api_key=\(API_KEY_V3)&language=en-US&page=\(page)") else { return }
        
        let session = URLSession(configuration: .default)
        dataTask = session.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
            
                self.output?.didReceivedError(errorString: "#request failed with error description: \(String(describing: error?.localizedDescription))")
            
            } else {
                
                let httpResponse = response as? HTTPURLResponse
                guard httpResponse?.statusCode == 200 else {
                    self.output?.didReceivedError(errorString: "#request failed with status: \(String(describing: httpResponse?.statusCode))")
                    return
                }
                
                if let a = try? JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let model = UpcomingMovieListModel.buildFrom(json: a!)
                    self.output?.didGetMoviesList(data: model)
                }
                
            }
        })

        dataTask?.resume()
        
    }
    
    func test() {
        
    }
    
}
