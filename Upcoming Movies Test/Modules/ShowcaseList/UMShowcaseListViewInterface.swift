//
//  UMShowcaseListPresenterViewInterface.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright (c) 2018 Leandro Lopes. All rights reserved.
//

import Foundation

protocol UMShowcaseListViewInterface: IndicatableView {

    
    /// Did get movies from request
    ///
    /// - Parameter data: a model that contains all info returned
    func didFetchMoviesList(data: UpcomingMovieListModel)
    
}
