//
//  UMProductDetailInteractor.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright (c) 2018 Leandro Lopes. All rights reserved.
//

import Foundation

// MARK: - UMProductDetailInteractorInput
protocol UMProductDetailInteractorInput: class {

    func getMovieDetails(movieId: Int)
    
}

// MARK: - UMProductDetailInteractorOutput
protocol UMProductDetailInteractorOutput: ViewInteractorOutput {

    func didGetMovieDetails(movie: UMMovieDetail)
    
}

// MARK: - UMProductDetailInteractor
class UMProductDetailInteractor {

    // MARK: Instance Variables
    weak var output: UMProductDetailInteractorOutput?
    fileprivate var dataTask: URLSessionDataTask?

}

// MARK: - Extension UMProductDetailInteractorInput
extension UMProductDetailInteractor: UMProductDetailInteractorInput {

    func getMovieDetails(movieId: Int) {
        
        guard let url = URL(string: "\(API_URL)movie/\(movieId)?api_key=\(API_KEY_V3)") else { return }
        
        let session = URLSession(configuration: .default)
        dataTask = session.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                
                self.output?.didReceivedError(errorString: "#request failed with error description: \(String(describing: error?.localizedDescription))")
                
            } else {
                
                let httpResponse = response as? HTTPURLResponse
                guard httpResponse?.statusCode == 200 else {
                    self.output?.didReceivedError(errorString: "#request failed with status: \(String(describing: httpResponse?.statusCode))")
                    return
                }
                
                if let a = try? JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let model = UMMovieDetail.buildFrom(json: a!)
                    self.output?.didGetMovieDetails(movie: model)
                }
            }
        })
        
        dataTask?.resume()
        
    }
    
}
