//
//  UMProductDetailPresenter.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright (c) 2018 Leandro Lopes. All rights reserved.
//

import Foundation

// MARK: - UMProductDetailPresenter
class UMProductDetailPresenter {

    // MARK: Instance Variables

    var interactor: UMProductDetailInteractorInput!
    weak var view: UMProductDetailViewInterface! {
        didSet{
            setupInterface()
        }
    }
    init(){}
    init(view: UMProductDetailViewInterface) {
        self.view = view
        setupInterface()
    }
    func setupInterface(){
        let thisInteractor = UMProductDetailInteractor()
        self.interactor = thisInteractor
        thisInteractor.output = self
    }

}

// MARK: - UMProductDetailModuleInterface
extension UMProductDetailPresenter: UMMovieDetailModuleInterface {
    
    func getMovieDetail(movieId: Int) {
        interactor.getMovieDetails(movieId: movieId)
    }

}

// MARK: - UMProductDetailInteractorOutput
extension UMProductDetailPresenter: UMProductDetailInteractorOutput {
    
    func didGetMovieDetails(movie: UMMovieDetail) {
        view.didFetchMovieDetails(movie: movie)
    }
    
    func didReceivedError(errorString: String) {
        
    }
    

}
