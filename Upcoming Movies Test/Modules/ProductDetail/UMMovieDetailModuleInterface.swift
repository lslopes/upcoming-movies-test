//
//  UMProductDetailModuleInterface.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright (c) 2018 Leandro Lopes. All rights reserved.
//

import Foundation

internal protocol UMMovieDetailModuleInterface: class {

    func getMovieDetail(movieId: Int)
    
}
