//
//  UMProductDetailViewInterface.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright (c) 2018 Leandro Lopes. All rights reserved.
//

import Foundation

protocol UMProductDetailViewInterface: IndicatableView {

    func didFetchMovieDetails(movie: UMMovieDetail)
    
}
