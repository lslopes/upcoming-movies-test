//
//  File.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import UIKit

protocol ViewInteractorOutput: class {
    func didReceivedError(errorString: String)
}

protocol IndicatableView: class {
    func showActivityIndicator()
    func hideActivityIndicator()
    func showIndicatableView(error: String)
}
