//
//  UMConstants.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import Foundation

enum ApiImageQuality: String {
    
    case low = "w154"
    case medium = "w342"
    case high = "w780"
    
}

var API_URL: String {
    return "https://api.themoviedb.org/3/"
}

var API_IMAGE: String {
    return "http://image.tmdb.org/t/p/"
}

var API_KEY_V3: String {
    return "d97ecaa400a63338575ff3709c2cac63"
}

var API_KEY_V4: String {
    return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkOTdlY2FhNDAwYTYzMzM4NTc1ZmYzNzA5YzJjYWM2MyIsInN1YiI6IjU5N2I2MGNkYzNhMzY4NjAwNzAwNjUwMiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.dIMZfIjWgx8Z6ONQCbteE1exjttLqE34Lp48wuV8YqI"
}

protocol UMNetworkServiceOutput {
    
    func didFetchUpcomingList(list: [UMUpcomingMovieModel])
    
}
