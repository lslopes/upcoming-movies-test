//
//  UMShowcaseMovieCollectionCell.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import UIKit

protocol UMShowcaseMovieCollectionCellProtocol {
    
    func config(movie: UMUpcomingMovieModel)
    
}

class UMShowcaseMovieCollectionCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var poster: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        poster.image = nil
    }

}

// MARK: UMShowcaseMovieCollectionCellProtocol
extension UMShowcaseMovieCollectionCell: UMShowcaseMovieCollectionCellProtocol {
    
    
    /// Setup the cell with movie model
    ///
    /// - Parameter movie: the movie model with all the data
    func config(movie: UMUpcomingMovieModel) {
        title.text = movie.title
        poster.loadImageUsingCache(withUrl: movie.poster_path, quality: .low)
    }
    
}
