//
//  UMShowcaseModel.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import Foundation

struct UMPagination: Decodable {
    
    var current = 0
    
    var next: Int {
        return current + 1
    }
    
    var previous: Int {
        return current - 1 >= 0 ? current - 1 : 0
    }
    
    mutating func setCurrent(page: Int) {
        self.current = page
    }
}

struct UpcomingMovieListModel {

    var pagination: UMPagination
    var results: [UMUpcomingMovieModel]
    var page: Int
    var total_results: Int
    var dates: [String]
    var total_pages: Int
    
    
    /// Receive an NSDictionary and returns a model.
    /// This model was used becouse i have some problem with Encoding/Decoding in this test
    /// and dont have time to fix
    ///
    /// - Parameter json: A Dictionay containing the data
    /// - Returns: return an struct value of UMShowcaseModel
    static func buildFrom(json: NSDictionary) -> UpcomingMovieListModel {
        return UpcomingMovieListModel(
            pagination: UMPagination(current: json["page"] as? Int ?? 1),
            results: UMUpcomingMovieModel.buildFrom(json: json["results"] as? [NSDictionary] ?? [NSDictionary()]),
            page: json["page"] as? Int ?? 1,
            total_results: json["total_results"] as? Int ?? 1,
            dates: json["dates"] as? [String] ?? [""],
            total_pages: json["total_pages"] as? Int ?? 0
        )
    }
    
}
