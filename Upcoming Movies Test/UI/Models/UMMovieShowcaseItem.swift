//
//  UMMovieDetail.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import Foundation


/// this model contains all data that upcoming call returns
struct UMUpcomingMovieModel {
    
    var vote_count: Int
    var id: Int
    var video: Bool
    var vote_average: Double
    var title: String
    var popularity: Double
    var poster_path: String
    var original_language: String
    var original_title: String
    var genre_ids:[Int]
    var backdrop_path: String
    var adult: Bool
    var overview: String
    var release_date: String
    
    
    /// Setup the model with json dictionary
    ///
    /// - Parameter json: the dictionary that returns in request
    /// - Returns: return array of UMUpcomingMovieModel model
    static func buildFrom(json: [NSDictionary]) -> [UMUpcomingMovieModel] {
        return json.map({ (json) -> UMUpcomingMovieModel in
            let model = UMUpcomingMovieModel(
                vote_count: json["vote_count"] as? Int ?? 0,
                id: json["id"] as? Int ?? 0,
                video: json["video"] as? Bool ?? false,
                vote_average: json["vote_average"] as? Double ?? 0,
                title: json["title"] as? String ?? "",
                popularity: json["popularity"] as? Double ?? 0,
                poster_path: json["poster_path"] as? String ?? "",
                original_language: json["original_language"] as? String ?? "",
                original_title: json["original_title"] as? String ?? "",
                genre_ids: json["genre_ids"] as? [Int] ?? [],
                backdrop_path: json["backdrop_path"] as? String ?? "",
                adult: json["adult"] as? Bool ?? false,
                overview: json["overview"] as? String ?? "",
                release_date: json["release_date"] as? String ?? ""
            )
            
            return model
        })
    }

}
