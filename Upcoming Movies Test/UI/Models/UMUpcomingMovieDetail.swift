//
//  UMUpcomingMovieDetail.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 30/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import Foundation


/// this model contains all the movie details
struct UMMovieDetail {
    
    var genres: [UMMovieGenre]
    
    static func buildFrom(json: NSDictionary) -> UMMovieDetail {
        return UMMovieDetail(
            genres: UMMovieGenre.buildFrom(json: json["genres"] as? [NSDictionary] ?? [NSDictionary()])
        )
        
    }
}
