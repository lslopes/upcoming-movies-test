//
//  UMMovieGenre.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 30/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import Foundation


/// This struc decribe one movie genre
struct UMMovieGenre {
    
    var id: Int
    var name: String
    
    static func buildFrom(json: [NSDictionary]) -> [UMMovieGenre] {
        return json.map({ (json) -> UMMovieGenre in
            let model = UMMovieGenre(
                id: json["id"] as? Int ?? 0,
                name: json["name"] as? String ?? ""
            )
            return model
        })
    }
    
}
