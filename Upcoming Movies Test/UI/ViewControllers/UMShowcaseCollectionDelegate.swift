//
//  UMShowcaseCollectionDelegate.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import UIKit

fileprivate enum UpcomingSection: Int {
    case movies = 0
    case loadMore = 1
    case none
}

protocol CollectionTouchProtocol: class {
    
    func didSelect(movie: UMUpcomingMovieModel)
    func loadMore()
    
}

class UMShowcaseCollectionDelegate: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var datasourse = [UMUpcomingMovieModel]()
    weak var collectionTouchDelegate: CollectionTouchProtocol?
    
    func clearDatasourse() {
        datasourse = [UMUpcomingMovieModel]()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let sectionType: UpcomingSection = UpcomingSection(rawValue: section) ?? .none
        switch sectionType {
        case .movies:
            return datasourse.count
        case .loadMore:
            return 1
        default:
            return 0
        }
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sectionType: UpcomingSection = UpcomingSection(rawValue: indexPath.section) ?? .none
        let availableScreenSize = UIScreen.main.bounds
        
        switch sectionType {
        case .movies:
            return CGSize(width: availableScreenSize.width * 0.48, height: 275)
            
        case .loadMore:
            return CGSize(width: availableScreenSize.width, height: 100)
            
        case .none:
            return CGSize.zero
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let sectionType: UpcomingSection = UpcomingSection(rawValue: indexPath.section) ?? .none
        
        switch sectionType {
        case .movies:
            let movie = datasourse[indexPath.row]
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: String(describing: UMShowcaseMovieCollectionCell.self),
                for: indexPath
                ) as! UMShowcaseMovieCollectionCell
            
            cell.config(movie: movie)
            return cell
            
        case .loadMore:
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: String(describing: UMLoadMoreCollectionViewCell.self),
                for: indexPath
            )
            
            collectionTouchDelegate?.loadMore()
            return cell
            
        default:
            return UICollectionViewCell()
        }
        
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sectionType: UpcomingSection = UpcomingSection(rawValue: indexPath.section) ?? .none
        
        switch sectionType {
        case .movies:
            let movie = datasourse[indexPath.row] 
            collectionTouchDelegate?.didSelect(movie: movie)
        
        case .loadMore:
            print("load more")
        
        default:
            print("default")
        }
            
        
    }
    
}
