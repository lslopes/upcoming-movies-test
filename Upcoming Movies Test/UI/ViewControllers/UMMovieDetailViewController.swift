//
//  UMMovieDetailViewController.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 30/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import UIKit

class UMMovieDetailViewController: UITableViewController {
    
    // Outlets
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var movieOverview: UITextView!
    @IBOutlet weak var movieReleaseDate: UILabel!
    // --
    
    lazy var movieDetailPresenter: UMProductDetailPresenter = {
        return UMProductDetailPresenter(view: self)
    }()
    
    var movie: UMUpcomingMovieModel?
    
    // MARK: Life Cicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        if let movieID = movie?.id {
            // i can get movie genre by getting all values before, but for
            // demonstration i'm using an presenter to get "all" movie details
            movieDetailPresenter.getMovieDetail(movieId: movieID)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateUI()
        
    }
    
    // MARK: Custom Methods
    func setupUI() {
        navigationItem.largeTitleDisplayMode = .never

    }
    
    
    /// Fill all the outlets
    func updateUI() {
        guard let movie = movie else { return }
        moviePoster.loadImageUsingCache(withUrl: movie.poster_path, quality: .medium)
        movieTitle.text = movie.title
        movieReleaseDate.text = movie.release_date
        movieOverview.text = movie.overview
        
    }
    
}

extension UMMovieDetailViewController: UMProductDetailViewInterface {
    
    func didFetchMovieDetails(movie: UMMovieDetail) {
        DispatchQueue.main.async {
            movie.genres.forEach { (genre) in
                self.movieGenre.text?.append(contentsOf: "\(genre.name) ")
            }
        }
    }
    
    func showActivityIndicator() {
        
    }
    
    func hideActivityIndicator() {
        
    }
    
    func showIndicatableView(error: String) {
        
    }
    
    
}
