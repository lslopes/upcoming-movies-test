//
//  UMMoviesListViewController.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 29/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import UIKit

class UMMoviesShowcaseViewController: UIViewController {
    
    @IBOutlet weak var collection: UICollectionView!
    
    fileprivate let collectionDelegate = UMShowcaseCollectionDelegate()
    fileprivate let segueDetail = "segueDetail"
    fileprivate var pagination = UMPagination(current: 0)
    fileprivate var searchController = UISearchController(searchResultsController: nil)
    

    /// this is the presenter, used in a MVP arquitechture
    /// https://pt.wikipedia.org/wiki/Model-view-presenter
    lazy var showcasePresenter: UMShowcaseListPresenter = {
        return UMShowcaseListPresenter(view: self)
    }()
    
    
    /// I have better plans for this, but no time
    fileprivate var upcomingMovieListModel: UpcomingMovieListModel?
    
    // MARK: Life Cicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showcasePresenter.getUpcomingMovies(page: pagination.next)
        setupUI()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueDetail {
            if let destination = segue.destination as? UMMovieDetailViewController {
                destination.movie = sender as? UMUpcomingMovieModel
            }
        }
    }
    
    // MARK: Custom Methods
    
    /// initial configuration settings for UI elements
    private func setupUI() {
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Movies"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.hidesSearchBarWhenScrolling = false
        
        title = "Upcoming Movies"
        
        collection.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collection.delegate = collectionDelegate
        collection.dataSource = collectionDelegate
        collectionDelegate.collectionTouchDelegate = self
        collection.register(
            UINib(nibName: String(describing: UMShowcaseMovieCollectionCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: UMShowcaseMovieCollectionCell.self)
        )
        collection.register(
            UINib(nibName: String(describing: UMLoadMoreCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: UMLoadMoreCollectionViewCell.self)
        )
    }
    
    private func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
}

extension UMMoviesShowcaseViewController: UMShowcaseListViewInterface {
    
    func didFetchMoviesList(data: UpcomingMovieListModel) {
        upcomingMovieListModel = data
        collectionDelegate.datasourse.append(contentsOf: data.results)
        DispatchQueue.main.async {
            self.collection.reloadData()
        }
    }
    
    func showActivityIndicator() {
        
    }
    
    func hideActivityIndicator() {
        
    }
    
    func showIndicatableView(error: String) {
        
    }
    
}

// MARK: CollectionTouchProtocol
extension UMMoviesShowcaseViewController: CollectionTouchProtocol {
    
    
    /// Return when user touch a collection cell
    ///
    /// - Parameter movie: the movie model
    func didSelect(movie: UMUpcomingMovieModel) {
        performSegue(withIdentifier: segueDetail, sender: movie)
    }
    
    
    /// Load more results
    func loadMore() {
        guard let next = upcomingMovieListModel?.pagination.next else { return }
        showcasePresenter.getUpcomingMovies(page: next)
    }
    
}

// MARK: UISearchResultsUpdating
extension UMMoviesShowcaseViewController: UISearchResultsUpdating {
    
    
    /// Just to demonstrate a search, not 100% but functional
    ///
    /// - Parameter searchController: the searchController used to control text input
    func updateSearchResults(for searchController: UISearchController) {
        upcomingMovieListModel = nil
        collectionDelegate.clearDatasourse()
        collection.reloadData()
        
        if searchBarIsEmpty() {
            showcasePresenter.getUpcomingMovies(page: 1)
        } else {
            let next = 1
            self.showcasePresenter.searchMovie(
                withTitle: searchController.searchBar.text!,
                page: next
            )
        }
    }
    
}
