//
//  UIImageExtension.swift
//  Upcoming Movies Test
//
//  Created by Leandro Lopes on 30/09/18.
//  Copyright © 2018 Leandro Lopes. All rights reserved.
//

import UIKit

let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
    func loadImageUsingCache(withUrl urlString : String, quality: ApiImageQuality) {
        let urlString = "\(API_IMAGE)/\(quality.rawValue)\(urlString)"
        let url = URL(string: urlString)
        self.image = nil
        
        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = cachedImage
            return
        }
        
        // if not, download image from url
        let loader = UIActivityIndicatorView(style: .gray)
        loader.hidesWhenStopped = true
        loader.center = center
        addSubview(loader)
        loader.startAnimating()
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                loader.removeFromSuperview()
            }
            
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                }
            }
            
        }).resume()
    }
}
